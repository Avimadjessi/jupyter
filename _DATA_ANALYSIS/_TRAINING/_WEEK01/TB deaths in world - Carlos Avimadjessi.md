
Deaths by tuberculosis
======

by Michel Wermelinger and Carlos Avimadjessi, 29 October 2015

This is the project notebook for Week 1 of The Open University's [_Learn to code for Data Analysis_](http://futurelearn.com/courses/learn-to-code) course.

In 2000, the United Nations set eight Millenium Development Goals (MDGs) to reduce poverty and diseases, improve gender equality and environmental sustainability, etc. Each goal is quantified and time-bound, to be achieved by the end of 2015. Goal 6 is to have halted and started reversing the spread of HIV, malaria and tuberculosis (TB).
TB doesn't make headlines like Ebola, SARS (severe acute respiratory syndrome) and other epidemics, but is far deadlier. For more information, see the World Health Organisation (WHO) page <http://www.who.int/gho/tb/en/>.

Given the population and number of deaths due to TB in some countries during one year, the following questions will be answered: 

>- What is the total, maximum, minimum and average number of deaths in that year?
- Which countries have the most and the least deaths?
- What is the death rate (deaths per 100,000 inhabitants) for each country?
- Which countries have the lowest and highest death rate?

The death rate allows for a better comparison of countries with widely different population sizes.

## The data

The data consists of total population and total number of deaths due to TB (excluding HIV) in 2013 in each of the BRICS and Portuguese-speaking countries. 

The data was taken from <http://apps.who.int/gho/data/node.main.POP107?lang=en> (population) and <http://apps.who.int/gho/data/node.main.593?lang=en> (deaths). The uncertainty bounds of the number of deaths were ignored.

The data was collected into an Excel file which should be in the same folder as this notebook.


```python
from pandas import *
data = read_excel('WHO POP TB all.xls')
data
```




<div style="max-height:1000px;max-width:1500px;overflow:auto;">
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Country</th>
      <th>Population (1000s)</th>
      <th>TB deaths</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0  </th>
      <td>                                       Afghanistan</td>
      <td>  30552</td>
      <td> 13000.00</td>
    </tr>
    <tr>
      <th>1  </th>
      <td>                                           Albania</td>
      <td>   3173</td>
      <td>    20.00</td>
    </tr>
    <tr>
      <th>2  </th>
      <td>                                           Algeria</td>
      <td>  39208</td>
      <td>  5100.00</td>
    </tr>
    <tr>
      <th>3  </th>
      <td>                                           Andorra</td>
      <td>     79</td>
      <td>     0.26</td>
    </tr>
    <tr>
      <th>4  </th>
      <td>                                            Angola</td>
      <td>  21472</td>
      <td>  6900.00</td>
    </tr>
    <tr>
      <th>5  </th>
      <td>                               Antigua and Barbuda</td>
      <td>     90</td>
      <td>     1.20</td>
    </tr>
    <tr>
      <th>6  </th>
      <td>                                         Argentina</td>
      <td>  41446</td>
      <td>   570.00</td>
    </tr>
    <tr>
      <th>7  </th>
      <td>                                           Armenia</td>
      <td>   2977</td>
      <td>   170.00</td>
    </tr>
    <tr>
      <th>8  </th>
      <td>                                         Australia</td>
      <td>  23343</td>
      <td>    45.00</td>
    </tr>
    <tr>
      <th>9  </th>
      <td>                                           Austria</td>
      <td>   8495</td>
      <td>    29.00</td>
    </tr>
    <tr>
      <th>10 </th>
      <td>                                        Azerbaijan</td>
      <td>   9413</td>
      <td>   360.00</td>
    </tr>
    <tr>
      <th>11 </th>
      <td>                                           Bahamas</td>
      <td>    377</td>
      <td>     1.80</td>
    </tr>
    <tr>
      <th>12 </th>
      <td>                                           Bahrain</td>
      <td>   1332</td>
      <td>     9.60</td>
    </tr>
    <tr>
      <th>13 </th>
      <td>                                        Bangladesh</td>
      <td> 156595</td>
      <td> 80000.00</td>
    </tr>
    <tr>
      <th>14 </th>
      <td>                                          Barbados</td>
      <td>    285</td>
      <td>     2.00</td>
    </tr>
    <tr>
      <th>15 </th>
      <td>                                           Belarus</td>
      <td>   9357</td>
      <td>   850.00</td>
    </tr>
    <tr>
      <th>16 </th>
      <td>                                           Belgium</td>
      <td>  11104</td>
      <td>    18.00</td>
    </tr>
    <tr>
      <th>17 </th>
      <td>                                            Belize</td>
      <td>    332</td>
      <td>    20.00</td>
    </tr>
    <tr>
      <th>18 </th>
      <td>                                             Benin</td>
      <td>  10323</td>
      <td>  1300.00</td>
    </tr>
    <tr>
      <th>19 </th>
      <td>                                            Bhutan</td>
      <td>    754</td>
      <td>    88.00</td>
    </tr>
    <tr>
      <th>20 </th>
      <td>                  Bolivia (Plurinational State of)</td>
      <td>  10671</td>
      <td>   430.00</td>
    </tr>
    <tr>
      <th>21 </th>
      <td>                            Bosnia and Herzegovina</td>
      <td>   3829</td>
      <td>   190.00</td>
    </tr>
    <tr>
      <th>22 </th>
      <td>                                          Botswana</td>
      <td>   2021</td>
      <td>   440.00</td>
    </tr>
    <tr>
      <th>23 </th>
      <td>                                            Brazil</td>
      <td> 200362</td>
      <td>  4400.00</td>
    </tr>
    <tr>
      <th>24 </th>
      <td>                                 Brunei Darussalam</td>
      <td>    418</td>
      <td>    13.00</td>
    </tr>
    <tr>
      <th>25 </th>
      <td>                                          Bulgaria</td>
      <td>   7223</td>
      <td>   150.00</td>
    </tr>
    <tr>
      <th>26 </th>
      <td>                                      Burkina Faso</td>
      <td>  16935</td>
      <td>  1500.00</td>
    </tr>
    <tr>
      <th>27 </th>
      <td>                                           Burundi</td>
      <td>  10163</td>
      <td>  2300.00</td>
    </tr>
    <tr>
      <th>28 </th>
      <td>                                    CÃ´te d'Ivoire</td>
      <td>  20316</td>
      <td>  4000.00</td>
    </tr>
    <tr>
      <th>29 </th>
      <td>                                        Cabo Verde</td>
      <td>    499</td>
      <td>   150.00</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>164</th>
      <td>                                          Suriname</td>
      <td>    539</td>
      <td>    12.00</td>
    </tr>
    <tr>
      <th>165</th>
      <td>                                         Swaziland</td>
      <td>   1250</td>
      <td>  1100.00</td>
    </tr>
    <tr>
      <th>166</th>
      <td>                                            Sweden</td>
      <td>   9571</td>
      <td>    13.00</td>
    </tr>
    <tr>
      <th>167</th>
      <td>                                       Switzerland</td>
      <td>   8078</td>
      <td>    17.00</td>
    </tr>
    <tr>
      <th>168</th>
      <td>                              Syrian Arab Republic</td>
      <td>  21898</td>
      <td>   450.00</td>
    </tr>
    <tr>
      <th>169</th>
      <td>                                        Tajikistan</td>
      <td>   8208</td>
      <td>   570.00</td>
    </tr>
    <tr>
      <th>170</th>
      <td>                                          Thailand</td>
      <td>  67010</td>
      <td>  8100.00</td>
    </tr>
    <tr>
      <th>171</th>
      <td>         The former Yugoslav republic of Macedonia</td>
      <td>   2107</td>
      <td>    33.00</td>
    </tr>
    <tr>
      <th>172</th>
      <td>                                       Timor-Leste</td>
      <td>   1133</td>
      <td>   990.00</td>
    </tr>
    <tr>
      <th>173</th>
      <td>                                              Togo</td>
      <td>   6817</td>
      <td>   810.00</td>
    </tr>
    <tr>
      <th>174</th>
      <td>                                             Tonga</td>
      <td>    105</td>
      <td>     2.50</td>
    </tr>
    <tr>
      <th>175</th>
      <td>                               Trinidad and Tobago</td>
      <td>   1341</td>
      <td>    29.00</td>
    </tr>
    <tr>
      <th>176</th>
      <td>                                           Tunisia</td>
      <td>  10997</td>
      <td>   230.00</td>
    </tr>
    <tr>
      <th>177</th>
      <td>                                            Turkey</td>
      <td>  74933</td>
      <td>   310.00</td>
    </tr>
    <tr>
      <th>178</th>
      <td>                                      Turkmenistan</td>
      <td>   5240</td>
      <td>  1300.00</td>
    </tr>
    <tr>
      <th>179</th>
      <td>                                            Tuvalu</td>
      <td>     10</td>
      <td>     2.80</td>
    </tr>
    <tr>
      <th>180</th>
      <td>                                            Uganda</td>
      <td>  37579</td>
      <td>  4100.00</td>
    </tr>
    <tr>
      <th>181</th>
      <td>                                           Ukraine</td>
      <td>  45239</td>
      <td>  6600.00</td>
    </tr>
    <tr>
      <th>182</th>
      <td>                              United Arab Emirates</td>
      <td>   9346</td>
      <td>    64.00</td>
    </tr>
    <tr>
      <th>183</th>
      <td> United Kingdom of Great Britain and Northern I...</td>
      <td>  63136</td>
      <td>   340.00</td>
    </tr>
    <tr>
      <th>184</th>
      <td>                       United Republic of Tanzania</td>
      <td>  49253</td>
      <td>  6000.00</td>
    </tr>
    <tr>
      <th>185</th>
      <td>                          United States of America</td>
      <td> 320051</td>
      <td>   490.00</td>
    </tr>
    <tr>
      <th>186</th>
      <td>                                           Uruguay</td>
      <td>   3407</td>
      <td>    40.00</td>
    </tr>
    <tr>
      <th>187</th>
      <td>                                        Uzbekistan</td>
      <td>  28934</td>
      <td>  2200.00</td>
    </tr>
    <tr>
      <th>188</th>
      <td>                                           Vanuatu</td>
      <td>    253</td>
      <td>    16.00</td>
    </tr>
    <tr>
      <th>189</th>
      <td>                Venezuela (Bolivarian Republic of)</td>
      <td>  30405</td>
      <td>   480.00</td>
    </tr>
    <tr>
      <th>190</th>
      <td>                                          Viet Nam</td>
      <td>  91680</td>
      <td> 17000.00</td>
    </tr>
    <tr>
      <th>191</th>
      <td>                                             Yemen</td>
      <td>  24407</td>
      <td>   990.00</td>
    </tr>
    <tr>
      <th>192</th>
      <td>                                            Zambia</td>
      <td>  14539</td>
      <td>  3600.00</td>
    </tr>
    <tr>
      <th>193</th>
      <td>                                          Zimbabwe</td>
      <td>  14150</td>
      <td>  5700.00</td>
    </tr>
  </tbody>
</table>
<p>194 rows × 3 columns</p>
</div>



## The range of the problem

The column of interest is the last one.


```python
tbColumn = data['TB deaths']
```

The total number of deaths in 2013 is:


```python
tbColumn.sum()
```




    1072677.97



The largest and smallest number of deaths in a single country are:


```python
tbColumn.max()
```




    240000.0




```python
tbColumn.min()
```




    0.0



From less than 20 to almost a quarter of a million deaths is a huge range. The average number of deaths, over all countries in the data, can give a better idea of the seriousness of the problem in each country.
The average can be computed as the mean or the median. Given the wide range of deaths, the median is probably a more sensible average measure.


```python
tbColumn.mean()
```




    5529.2678865979378




```python
tbColumn.median()
```




    315.0



The median is far lower than the mean. This indicates that some of the countries had a very high number of TB deaths in 2013, pushing the value of the mean up.

## The most affected

To see the most affected countries, the table is sorted in ascending order by the last column, which puts those countries in the last rows.


```python
data.sort('TB deaths')
```




<div style="max-height:1000px;max-width:1500px;overflow:auto;">
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Country</th>
      <th>Population (1000s)</th>
      <th>TB deaths</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>147</th>
      <td>                            San Marino</td>
      <td>      31</td>
      <td>      0.00</td>
    </tr>
    <tr>
      <th>125</th>
      <td>                                  Niue</td>
      <td>       1</td>
      <td>      0.01</td>
    </tr>
    <tr>
      <th>111</th>
      <td>                                Monaco</td>
      <td>      38</td>
      <td>      0.03</td>
    </tr>
    <tr>
      <th>3  </th>
      <td>                               Andorra</td>
      <td>      79</td>
      <td>      0.26</td>
    </tr>
    <tr>
      <th>129</th>
      <td>                                 Palau</td>
      <td>      21</td>
      <td>      0.36</td>
    </tr>
    <tr>
      <th>40 </th>
      <td>                          Cook Islands</td>
      <td>      21</td>
      <td>      0.41</td>
    </tr>
    <tr>
      <th>118</th>
      <td>                                 Nauru</td>
      <td>      10</td>
      <td>      0.67</td>
    </tr>
    <tr>
      <th>76 </th>
      <td>                               Iceland</td>
      <td>     330</td>
      <td>      0.93</td>
    </tr>
    <tr>
      <th>68 </th>
      <td>                               Grenada</td>
      <td>     106</td>
      <td>      1.10</td>
    </tr>
    <tr>
      <th>5  </th>
      <td>                   Antigua and Barbuda</td>
      <td>      90</td>
      <td>      1.20</td>
    </tr>
    <tr>
      <th>113</th>
      <td>                            Montenegro</td>
      <td>     621</td>
      <td>      1.20</td>
    </tr>
    <tr>
      <th>152</th>
      <td>                            Seychelles</td>
      <td>      93</td>
      <td>      1.40</td>
    </tr>
    <tr>
      <th>105</th>
      <td>                                 Malta</td>
      <td>     429</td>
      <td>      1.50</td>
    </tr>
    <tr>
      <th>143</th>
      <td>                 Saint Kitts and Nevis</td>
      <td>      54</td>
      <td>      1.60</td>
    </tr>
    <tr>
      <th>11 </th>
      <td>                               Bahamas</td>
      <td>     377</td>
      <td>      1.80</td>
    </tr>
    <tr>
      <th>14 </th>
      <td>                              Barbados</td>
      <td>     285</td>
      <td>      2.00</td>
    </tr>
    <tr>
      <th>144</th>
      <td>                           Saint Lucia</td>
      <td>     182</td>
      <td>      2.20</td>
    </tr>
    <tr>
      <th>99 </th>
      <td>                            Luxembourg</td>
      <td>     530</td>
      <td>      2.20</td>
    </tr>
    <tr>
      <th>44 </th>
      <td>                                Cyprus</td>
      <td>    1141</td>
      <td>      2.30</td>
    </tr>
    <tr>
      <th>174</th>
      <td>                                 Tonga</td>
      <td>     105</td>
      <td>      2.50</td>
    </tr>
    <tr>
      <th>50 </th>
      <td>                              Dominica</td>
      <td>      72</td>
      <td>      2.70</td>
    </tr>
    <tr>
      <th>137</th>
      <td>                                 Qatar</td>
      <td>    2169</td>
      <td>      2.70</td>
    </tr>
    <tr>
      <th>179</th>
      <td>                                Tuvalu</td>
      <td>      10</td>
      <td>      2.80</td>
    </tr>
    <tr>
      <th>145</th>
      <td>      Saint Vincent and the Grenadines</td>
      <td>     109</td>
      <td>      3.10</td>
    </tr>
    <tr>
      <th>126</th>
      <td>                                Norway</td>
      <td>    5043</td>
      <td>      4.40</td>
    </tr>
    <tr>
      <th>146</th>
      <td>                                 Samoa</td>
      <td>     190</td>
      <td>      6.10</td>
    </tr>
    <tr>
      <th>121</th>
      <td>                           New Zealand</td>
      <td>    4506</td>
      <td>      6.30</td>
    </tr>
    <tr>
      <th>103</th>
      <td>                              Maldives</td>
      <td>     345</td>
      <td>      7.60</td>
    </tr>
    <tr>
      <th>12 </th>
      <td>                               Bahrain</td>
      <td>    1332</td>
      <td>      9.60</td>
    </tr>
    <tr>
      <th>164</th>
      <td>                              Suriname</td>
      <td>     539</td>
      <td>     12.00</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>160</th>
      <td>                           South Sudan</td>
      <td>   11296</td>
      <td>   4500.00</td>
    </tr>
    <tr>
      <th>119</th>
      <td>                                 Nepal</td>
      <td>   27797</td>
      <td>   4600.00</td>
    </tr>
    <tr>
      <th>2  </th>
      <td>                               Algeria</td>
      <td>   39208</td>
      <td>   5100.00</td>
    </tr>
    <tr>
      <th>193</th>
      <td>                              Zimbabwe</td>
      <td>   14150</td>
      <td>   5700.00</td>
    </tr>
    <tr>
      <th>184</th>
      <td>           United Republic of Tanzania</td>
      <td>   49253</td>
      <td>   6000.00</td>
    </tr>
    <tr>
      <th>181</th>
      <td>                               Ukraine</td>
      <td>   45239</td>
      <td>   6600.00</td>
    </tr>
    <tr>
      <th>46 </th>
      <td> Democratic People's Republic of Korea</td>
      <td>   24895</td>
      <td>   6700.00</td>
    </tr>
    <tr>
      <th>4  </th>
      <td>                                Angola</td>
      <td>   21472</td>
      <td>   6900.00</td>
    </tr>
    <tr>
      <th>158</th>
      <td>                               Somalia</td>
      <td>   10496</td>
      <td>   7700.00</td>
    </tr>
    <tr>
      <th>31 </th>
      <td>                              Cameroon</td>
      <td>   22254</td>
      <td>   7800.00</td>
    </tr>
    <tr>
      <th>170</th>
      <td>                              Thailand</td>
      <td>   67010</td>
      <td>   8100.00</td>
    </tr>
    <tr>
      <th>88 </th>
      <td>                                 Kenya</td>
      <td>   44354</td>
      <td>   9100.00</td>
    </tr>
    <tr>
      <th>163</th>
      <td>                                 Sudan</td>
      <td>   37964</td>
      <td>   9700.00</td>
    </tr>
    <tr>
      <th>30 </th>
      <td>                              Cambodia</td>
      <td>   15135</td>
      <td>  10000.00</td>
    </tr>
    <tr>
      <th>100</th>
      <td>                            Madagascar</td>
      <td>   22925</td>
      <td>  12000.00</td>
    </tr>
    <tr>
      <th>0  </th>
      <td>                           Afghanistan</td>
      <td>   30552</td>
      <td>  13000.00</td>
    </tr>
    <tr>
      <th>141</th>
      <td>                    Russian Federation</td>
      <td>  142834</td>
      <td>  17000.00</td>
    </tr>
    <tr>
      <th>190</th>
      <td>                              Viet Nam</td>
      <td>   91680</td>
      <td>  17000.00</td>
    </tr>
    <tr>
      <th>115</th>
      <td>                            Mozambique</td>
      <td>   25834</td>
      <td>  18000.00</td>
    </tr>
    <tr>
      <th>159</th>
      <td>                          South Africa</td>
      <td>   52776</td>
      <td>  25000.00</td>
    </tr>
    <tr>
      <th>116</th>
      <td>                               Myanmar</td>
      <td>   53259</td>
      <td>  26000.00</td>
    </tr>
    <tr>
      <th>134</th>
      <td>                           Philippines</td>
      <td>   98394</td>
      <td>  27000.00</td>
    </tr>
    <tr>
      <th>58 </th>
      <td>                              Ethiopia</td>
      <td>   94101</td>
      <td>  30000.00</td>
    </tr>
    <tr>
      <th>36 </th>
      <td>                                 China</td>
      <td> 1393337</td>
      <td>  41000.00</td>
    </tr>
    <tr>
      <th>47 </th>
      <td>      Democratic Republic of the Congo</td>
      <td>   67514</td>
      <td>  46000.00</td>
    </tr>
    <tr>
      <th>128</th>
      <td>                              Pakistan</td>
      <td>  182143</td>
      <td>  49000.00</td>
    </tr>
    <tr>
      <th>78 </th>
      <td>                             Indonesia</td>
      <td>  249866</td>
      <td>  64000.00</td>
    </tr>
    <tr>
      <th>13 </th>
      <td>                            Bangladesh</td>
      <td>  156595</td>
      <td>  80000.00</td>
    </tr>
    <tr>
      <th>124</th>
      <td>                               Nigeria</td>
      <td>  173615</td>
      <td> 160000.00</td>
    </tr>
    <tr>
      <th>77 </th>
      <td>                                 India</td>
      <td> 1252140</td>
      <td> 240000.00</td>
    </tr>
  </tbody>
</table>
<p>194 rows × 3 columns</p>
</div>



The table raises the possibility that a large number of deaths may be partly due to a large population. To compare the countries on an equal footing, the death rate per 100,000 inhabitants is computed and sorted in order to easily see the lowest and highest rate.


```python
populationColumn = data['Population (1000s)']
data['TB deaths (per 100,000)'] = tbColumn * 100 / populationColumn
data.sort('TB deaths (per 100,000)')
```




<div style="max-height:1000px;max-width:1500px;overflow:auto;">
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Country</th>
      <th>Population (1000s)</th>
      <th>TB deaths</th>
      <th>TB deaths (per 100,000)</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>147</th>
      <td>                       San Marino</td>
      <td>     31</td>
      <td>      0.00</td>
      <td>  0.000000</td>
    </tr>
    <tr>
      <th>111</th>
      <td>                           Monaco</td>
      <td>     38</td>
      <td>      0.03</td>
      <td>  0.078947</td>
    </tr>
    <tr>
      <th>126</th>
      <td>                           Norway</td>
      <td>   5043</td>
      <td>      4.40</td>
      <td>  0.087250</td>
    </tr>
    <tr>
      <th>120</th>
      <td>                      Netherlands</td>
      <td>  16759</td>
      <td>     20.00</td>
      <td>  0.119339</td>
    </tr>
    <tr>
      <th>137</th>
      <td>                            Qatar</td>
      <td>   2169</td>
      <td>      2.70</td>
      <td>  0.124481</td>
    </tr>
    <tr>
      <th>166</th>
      <td>                           Sweden</td>
      <td>   9571</td>
      <td>     13.00</td>
      <td>  0.135827</td>
    </tr>
    <tr>
      <th>121</th>
      <td>                      New Zealand</td>
      <td>   4506</td>
      <td>      6.30</td>
      <td>  0.139814</td>
    </tr>
    <tr>
      <th>185</th>
      <td>         United States of America</td>
      <td> 320051</td>
      <td>    490.00</td>
      <td>  0.153101</td>
    </tr>
    <tr>
      <th>16 </th>
      <td>                          Belgium</td>
      <td>  11104</td>
      <td>     18.00</td>
      <td>  0.162104</td>
    </tr>
    <tr>
      <th>32 </th>
      <td>                           Canada</td>
      <td>  35182</td>
      <td>     62.00</td>
      <td>  0.176226</td>
    </tr>
    <tr>
      <th>8  </th>
      <td>                        Australia</td>
      <td>  23343</td>
      <td>     45.00</td>
      <td>  0.192777</td>
    </tr>
    <tr>
      <th>113</th>
      <td>                       Montenegro</td>
      <td>    621</td>
      <td>      1.20</td>
      <td>  0.193237</td>
    </tr>
    <tr>
      <th>44 </th>
      <td>                           Cyprus</td>
      <td>   1141</td>
      <td>      2.30</td>
      <td>  0.201578</td>
    </tr>
    <tr>
      <th>82 </th>
      <td>                           Israel</td>
      <td>   7733</td>
      <td>     16.00</td>
      <td>  0.206905</td>
    </tr>
    <tr>
      <th>167</th>
      <td>                      Switzerland</td>
      <td>   8078</td>
      <td>     17.00</td>
      <td>  0.210448</td>
    </tr>
    <tr>
      <th>45 </th>
      <td>                   Czech Republic</td>
      <td>  10702</td>
      <td>     28.00</td>
      <td>  0.261633</td>
    </tr>
    <tr>
      <th>76 </th>
      <td>                          Iceland</td>
      <td>    330</td>
      <td>      0.93</td>
      <td>  0.281818</td>
    </tr>
    <tr>
      <th>60 </th>
      <td>                          Finland</td>
      <td>   5426</td>
      <td>     17.00</td>
      <td>  0.313306</td>
    </tr>
    <tr>
      <th>43 </th>
      <td>                             Cuba</td>
      <td>  11266</td>
      <td>     37.00</td>
      <td>  0.328422</td>
    </tr>
    <tr>
      <th>3  </th>
      <td>                          Andorra</td>
      <td>     79</td>
      <td>      0.26</td>
      <td>  0.329114</td>
    </tr>
    <tr>
      <th>9  </th>
      <td>                          Austria</td>
      <td>   8495</td>
      <td>     29.00</td>
      <td>  0.341377</td>
    </tr>
    <tr>
      <th>105</th>
      <td>                            Malta</td>
      <td>    429</td>
      <td>      1.50</td>
      <td>  0.349650</td>
    </tr>
    <tr>
      <th>65 </th>
      <td>                          Germany</td>
      <td>  82727</td>
      <td>    300.00</td>
      <td>  0.362639</td>
    </tr>
    <tr>
      <th>81 </th>
      <td>                          Ireland</td>
      <td>   4627</td>
      <td>     18.00</td>
      <td>  0.389021</td>
    </tr>
    <tr>
      <th>177</th>
      <td>                           Turkey</td>
      <td>  74933</td>
      <td>    310.00</td>
      <td>  0.413703</td>
    </tr>
    <tr>
      <th>99 </th>
      <td>                       Luxembourg</td>
      <td>    530</td>
      <td>      2.20</td>
      <td>  0.415094</td>
    </tr>
    <tr>
      <th>48 </th>
      <td>                          Denmark</td>
      <td>   5619</td>
      <td>     24.00</td>
      <td>  0.427122</td>
    </tr>
    <tr>
      <th>11 </th>
      <td>                          Bahamas</td>
      <td>    377</td>
      <td>      1.80</td>
      <td>  0.477454</td>
    </tr>
    <tr>
      <th>86 </th>
      <td>                           Jordan</td>
      <td>   7274</td>
      <td>     35.00</td>
      <td>  0.481166</td>
    </tr>
    <tr>
      <th>83 </th>
      <td>                            Italy</td>
      <td>  60990</td>
      <td>    310.00</td>
      <td>  0.508280</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>29 </th>
      <td>                       Cabo Verde</td>
      <td>    499</td>
      <td>    150.00</td>
      <td> 30.060120</td>
    </tr>
    <tr>
      <th>58 </th>
      <td>                         Ethiopia</td>
      <td>  94101</td>
      <td>  30000.00</td>
      <td> 31.880639</td>
    </tr>
    <tr>
      <th>4  </th>
      <td>                           Angola</td>
      <td>  21472</td>
      <td>   6900.00</td>
      <td> 32.134873</td>
    </tr>
    <tr>
      <th>131</th>
      <td>                 Papua New Guinea</td>
      <td>   7321</td>
      <td>   2400.00</td>
      <td> 32.782407</td>
    </tr>
    <tr>
      <th>31 </th>
      <td>                         Cameroon</td>
      <td>  22254</td>
      <td>   7800.00</td>
      <td> 35.049879</td>
    </tr>
    <tr>
      <th>106</th>
      <td>                 Marshall Islands</td>
      <td>     53</td>
      <td>     21.00</td>
      <td> 39.622642</td>
    </tr>
    <tr>
      <th>160</th>
      <td>                      South Sudan</td>
      <td>  11296</td>
      <td>   4500.00</td>
      <td> 39.837110</td>
    </tr>
    <tr>
      <th>193</th>
      <td>                         Zimbabwe</td>
      <td>  14150</td>
      <td>   5700.00</td>
      <td> 40.282686</td>
    </tr>
    <tr>
      <th>0  </th>
      <td>                      Afghanistan</td>
      <td>  30552</td>
      <td>  13000.00</td>
      <td> 42.550406</td>
    </tr>
    <tr>
      <th>153</th>
      <td>                     Sierra Leone</td>
      <td>   6092</td>
      <td>   2600.00</td>
      <td> 42.678923</td>
    </tr>
    <tr>
      <th>39 </th>
      <td>                            Congo</td>
      <td>   4448</td>
      <td>   2000.00</td>
      <td> 44.964029</td>
    </tr>
    <tr>
      <th>95 </th>
      <td>                          Lesotho</td>
      <td>   2074</td>
      <td>    960.00</td>
      <td> 46.287367</td>
    </tr>
    <tr>
      <th>159</th>
      <td>                     South Africa</td>
      <td>  52776</td>
      <td>  25000.00</td>
      <td> 47.370017</td>
    </tr>
    <tr>
      <th>33 </th>
      <td>         Central African Republic</td>
      <td>   4616</td>
      <td>   2200.00</td>
      <td> 47.660312</td>
    </tr>
    <tr>
      <th>116</th>
      <td>                          Myanmar</td>
      <td>  53259</td>
      <td>  26000.00</td>
      <td> 48.818040</td>
    </tr>
    <tr>
      <th>96 </th>
      <td>                          Liberia</td>
      <td>   4294</td>
      <td>   2100.00</td>
      <td> 48.905449</td>
    </tr>
    <tr>
      <th>13 </th>
      <td>                       Bangladesh</td>
      <td> 156595</td>
      <td>  80000.00</td>
      <td> 51.087199</td>
    </tr>
    <tr>
      <th>100</th>
      <td>                       Madagascar</td>
      <td>  22925</td>
      <td>  12000.00</td>
      <td> 52.344602</td>
    </tr>
    <tr>
      <th>92 </th>
      <td> Lao People's Democratic Republic</td>
      <td>   6770</td>
      <td>   3600.00</td>
      <td> 53.175775</td>
    </tr>
    <tr>
      <th>62 </th>
      <td>                            Gabon</td>
      <td>   1672</td>
      <td>    910.00</td>
      <td> 54.425837</td>
    </tr>
    <tr>
      <th>117</th>
      <td>                          Namibia</td>
      <td>   2303</td>
      <td>   1300.00</td>
      <td> 56.448111</td>
    </tr>
    <tr>
      <th>30 </th>
      <td>                         Cambodia</td>
      <td>  15135</td>
      <td>  10000.00</td>
      <td> 66.072019</td>
    </tr>
    <tr>
      <th>47 </th>
      <td> Democratic Republic of the Congo</td>
      <td>  67514</td>
      <td>  46000.00</td>
      <td> 68.134017</td>
    </tr>
    <tr>
      <th>115</th>
      <td>                       Mozambique</td>
      <td>  25834</td>
      <td>  18000.00</td>
      <td> 69.675621</td>
    </tr>
    <tr>
      <th>71 </th>
      <td>                    Guinea-Bissau</td>
      <td>   1704</td>
      <td>   1200.00</td>
      <td> 70.422535</td>
    </tr>
    <tr>
      <th>158</th>
      <td>                          Somalia</td>
      <td>  10496</td>
      <td>   7700.00</td>
      <td> 73.361280</td>
    </tr>
    <tr>
      <th>172</th>
      <td>                      Timor-Leste</td>
      <td>   1133</td>
      <td>    990.00</td>
      <td> 87.378641</td>
    </tr>
    <tr>
      <th>165</th>
      <td>                        Swaziland</td>
      <td>   1250</td>
      <td>   1100.00</td>
      <td> 88.000000</td>
    </tr>
    <tr>
      <th>124</th>
      <td>                          Nigeria</td>
      <td> 173615</td>
      <td> 160000.00</td>
      <td> 92.157936</td>
    </tr>
    <tr>
      <th>49 </th>
      <td>                         Djibouti</td>
      <td>    873</td>
      <td>    870.00</td>
      <td> 99.656357</td>
    </tr>
  </tbody>
</table>
<p>194 rows × 4 columns</p>
</div>



## Conclusions

The world had a total of more than 1 million ( 1 072 678) deaths due to TB in 2013. 
The median shows that half of these coutries had fewer than 315 deaths. The much higher mean (over 5529) indicates that some countries had a very high number. The least affected were San Marino, Niue and Monaco with 1 death each, and the most affected were Nigeria and India with 160 thousand and 240 thousand deaths in a single year. However, taking the population size into account, the least affected were San Marino and Monaco with no death per 100 thousand inhabitants, and the most affected were Nigeria and Djibouti with over 92 deaths per 100,000 inhabitants.

One should not forget that most values are estimates, and that the chosen countries are a small sample of all the world's countries. Nevertheless, they convey the message that TB is still a major cause of fatalities, and that there is a huge disparity between countries, with several ones being highly affected.
